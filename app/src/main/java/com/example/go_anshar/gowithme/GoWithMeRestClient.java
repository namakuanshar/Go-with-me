package com.example.go_anshar.gowithme;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.*;

import cz.msebera.android.httpclient.*;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by jeje on 7/17/17.
 */

public class GoWithMeRestClient {

        private static final String BASE_URL = "https://go-with-me-application.herokuapp.com/v1/";

        private static AsyncHttpClient client = new AsyncHttpClient();
        private static SyncHttpClient client2 = new SyncHttpClient();

        public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
            client.get(getAbsoluteUrl(url), params, responseHandler);
        }

        public static void getJson(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
            client.get(getAbsoluteUrl(url), params, responseHandler);
        }

        public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
            Log.d("URL: ", getAbsoluteUrl(url));
            client.post(getAbsoluteUrl(url), params, responseHandler);

        }

        public static void postJson(Context context, String url, StringEntity entity, AsyncHttpResponseHandler responseHandler) {
            Log.d("URL: ", getAbsoluteUrl(url));
            client2.post(context, getAbsoluteUrl(url), entity, "application/json", responseHandler);

        }

        private static String getAbsoluteUrl(String relativeUrl) {
            return BASE_URL + relativeUrl;
        }
}
