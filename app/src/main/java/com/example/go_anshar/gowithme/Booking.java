package com.example.go_anshar.gowithme;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by jeje on 7/17/17.
 */

public class Booking {

    private String booking_id;
    private String status;

    public void create(Context context, String customer_id, String fare, String pick_up, String destination) {

        GoWithMeRestClient restClient = new GoWithMeRestClient();
        JSONObject jdata = new JSONObject();
        StringEntity entity = null;
        booking_id = "";
        try {
            jdata.put("customer_id", customer_id);
            jdata.put("fare", fare);
            jdata.put("pick_up", pick_up);
            jdata.put("destination", destination);
        } catch (JSONException ex) {
            // json exception
        }

        try {
            entity = new StringEntity(jdata.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {
            restClient.postJson(context, "booking/create", entity, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    //Log.d("Status code: ", "" + statusCode);
                    JSONObject response = null;
                    try {
                        response = new JSONObject(new String(responseBody));
                        booking_id = response.getString("booking_id");

                        Log.d("Sukses Booking id: ", "" + booking_id);
                        setBooking_id(booking_id);
                    } catch (Exception e) {}
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.d("Failed: ", ""+statusCode);
                    Log.d("Error : ", "" + error);
                }
            });
        }
        catch(Exception e) {
            e.printStackTrace();
        }

    }

    public String getStatus(Context context, final String booking_id) {

        Log.d("Find driver for bid : ", booking_id);

        GoWithMeRestClient restClient = new GoWithMeRestClient();

        try {
            restClient.getJson("booking/status?booking_id=" + booking_id, new RequestParams(), new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    //Log.d("Status code: ", "" + statusCode);
                    JSONObject response = null;
                    try {
                        response = new JSONObject(new String(responseBody));
                        status = response.getString("status");
                        Log.d("Booking id: ", "" + booking_id);
                        Log.d("Sukses Booking status: ", "" + status);
                    } catch (Exception e) {}
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.d("Failed: ", ""+statusCode);
                    Log.d("Error : ", "" + error);
                }
            });
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public String getBooking_id() {
        return this.booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

}
