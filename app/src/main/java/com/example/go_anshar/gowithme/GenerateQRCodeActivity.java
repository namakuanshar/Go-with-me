package com.example.go_anshar.gowithme;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.zxing.WriterException;
import com.loopj.android.http.*;


public class GenerateQRCodeActivity extends AppCompatActivity {


    ImageView imageView;
    Button button;
    Button scanButton;
    FrameLayout layout_progress;
    EditText editText;
    String EditTextValue;
    GenerateQRCode generateQRCode;
    Bitmap bitmap;
    Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        generateQRCode = new GenerateQRCode(getResources());

        setContentView(R.layout.generate_qr_code);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String message = intent.getStringExtra(MapsActivity.EXTRA_MESSAGE);

        imageView = (ImageView) findViewById(R.id.imageView);

        AsyncTaskRunner runner = new AsyncTaskRunner();
        runner.setResource(getResources());
        runner.setEditTextValue(message);
        runner.setImageView(imageView);
        runner.execute();
    }


    class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;
        String EditTextValue;
        Bitmap bitmap;
        GenerateQRCode generateQRCode;
        ImageView imageView;

        private void setResource(Resources r) {
            generateQRCode = new GenerateQRCode(r);
        }

        public void setEditTextValue(String EditTextValue) {
            this.EditTextValue = EditTextValue;
        }

        public void setImageView(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                bitmap = generateQRCode.TextToImageEncode(EditTextValue);

            } catch (WriterException e) {
                e.printStackTrace();
            }
            return EditTextValue;
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            imageView.setImageBitmap(bitmap);
            progressDialog.dismiss();

            //Trip t = new Trip();
            //t.findDriver(c, result);
        }


        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(GenerateQRCodeActivity.this,
                    "Loading",
                    "Generating the QR Code");
        }


        @Override
        protected void onProgressUpdate(String... text) {
            //finalResult.setText(text[0]);
            // Things to be done while execution of long running operation is in
            // progress. For example updating ProgessDialog
        }
    }

}








