package com.example.go_anshar.gowithme;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.vision.text.Text;

import android.support.v4.app.FragmentActivity;
import android.widget.TextView;
import android.widget.Toast;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    Context context;
    public static final String EXTRA_MESSAGE = "";
    LatLng latLngA = null, latLngB = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        context = this.getApplicationContext();

        //buttonWithME
        Button WithMe = (Button) findViewById(R.id.buttonWithMe);
        WithMe.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                jalan();
            }
        });

        //button jarak
//        Button btnJarak = (Button) findViewById(R.id.buttonJarak);
//        btnJarak.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                jarak();
//            }
//        });

        //TODO : clear the previus marker(if nesesssary)

        //pick place
        PlaceAutocompleteFragment placePick =
                (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.fragment_pick);
        placePick.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place)
            {
                System.out.println(place.getLatLng());
                latLngA = place.getLatLng();
                mMap.addMarker(new MarkerOptions().position(latLngA).title("Pick Place"));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngA, 15));
            }

            @Override
            public void onError(Status status)
            {
                System.out.println(status);
            }
        });

        //Destination Place
        PlaceAutocompleteFragment destinationPick =
                (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.fragment_destination);
        destinationPick.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place)
            {
                System.out.println(place.getLatLng());
                latLngB = place.getLatLng();
                mMap.addMarker(new MarkerOptions().position(latLngB).title("Pick Place"));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngB, 15));
            }

            @Override
            public void onError(Status status)
            {
                System.out.println(status);
            }
        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    //go function
    public void jalan(){
        if((latLngA == null) || (latLngB == null))  {
            Toast.makeText(this, "Please select the address", Toast.LENGTH_SHORT).show();
        } else {
            // Generate QR Code
            MapsActivity.AsyncTaskRunner runner = new MapsActivity.AsyncTaskRunner();
            runner.execute();
        }
    }

    public double calculateDistance(LatLng pickup, LatLng destination) {

        final double LATITUDE_TO_KM = 110.574;
        final double PICKUP_LONGITUDE_TO_KM = 111.320 * Math.cos(pickup.latitude);
        final double DESTINATION_LONGITUDE_TO_KM = 111.320 * Math.cos(destination.latitude);

        double firstVector = Math.pow((pickup.latitude * LATITUDE_TO_KM - pickup.longitude
                * PICKUP_LONGITUDE_TO_KM ), 2);
        double secondVector = Math.pow((destination.latitude * LATITUDE_TO_KM - destination.longitude
                * DESTINATION_LONGITUDE_TO_KM), 2);
        double result = Math.floor(Math.sqrt((firstVector + secondVector)));

        return result;
    }


//    public void showDistance(View view) {
//        double result = calculateDistance(pickup, destination);
//        Toast.makeText(this, "hasilnya : " + result + " km", Toast.LENGTH_LONG).show();
//    }

    //find distance
    public void jarak(View view){
        Location locationA = new Location("point A");
        locationA.setLatitude(latLngA.latitude);
        locationA.setLongitude(latLngA.longitude);
        Location locationB = new Location("point B");
        locationB.setLatitude(latLngB.latitude);
        locationB.setLongitude(latLngB.longitude);

        //distance in km (/1000)
        double distance = (locationA.distanceTo(locationB) / 1000);
        int totalDistance = (int) Math.round(distance);
        int totalPrice = totalDistance * 500;

        String printedDistance = "Distance: " + totalDistance + " km";
        String printedPrice = "Price: Rp " + totalPrice;

        Button updateDistance = (Button) findViewById(R.id.buttonJarak);
        TextView distanceDisplayed = (TextView) findViewById(R.id.textView2);
        TextView priceDisplayed = (TextView) findViewById(R.id.textView);
        distanceDisplayed.setText(printedDistance);
        priceDisplayed.setText(printedPrice);
//        Toast.makeText(this, "Jarak :"+distance+" km", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marke"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
    }


    class AsyncTaskRunner extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;
        Booking booking;
        String booking_id;

        @Override
        protected String doInBackground(String... params) {
            booking = new Booking();

            publishProgress("Sleeping..."); // Calls onProgressUpdate()

            booking.create(context, "123", "1000.9", latLngA.toString(), latLngB.toString());
            booking_id = booking.getBooking_id();
            Log.d("The booking id", booking_id);
            return booking_id;
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            Intent intent = new Intent(MapsActivity.this, GenerateQRCodeActivity.class);
            intent.putExtra(EXTRA_MESSAGE,booking_id);
            startActivity(intent);

        }


        @Override
        protected void onPreExecute() {
        }


        @Override
        protected void onProgressUpdate(String... text) {
            //finalResult.setText(text[0]);
            // Things to be done while execution of long running operation is in
            // progress. For example updating ProgessDialog
        }
    }

}
